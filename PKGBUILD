# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Maxim Kurnosenko <asusx2@mail.ru>
# Contributor: Salvador Pardiñas <darkfm@vera.com.uy>

pkgname=woeusb-git
pkgver=3.3.1.r0
pkgrel=3.1
pkgdesc="A Linux program to create Windows USB stick installer from a real Windows DVD or an image"
arch=('i686' 'x86_64')
url="https://github.com/slacka/WoeUSB"
license=('GPL3')
makedepends=('autoconf' 'git' 'make' 'wxgtk3')
provides=('woeusb')
conflicts=('woeusb')
source=("git+https://github.com/slacka/WoeUSB.git#tag=v3.3.1"
        "https://metainfo.manjariando.com.br/${pkgname}/com.woeusbgui.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/woeusbgui"-{48,64,128}.png)
sha256sums=('SKIP'
            'af6413eb589992b915c3ea626e6ee64d91fc6bbf78464debdd8256e356836923'
            'd630c1a1627a739cd019875439b489ad95645fcefa448239b02ebcd8bd4faa83'
            '8ce765c50fdb77714b54d7f64c9f897204e244d433a83d20d594e8225c6d39f5'
            '9095c9d80c247dad6b2ad53d150c0cae1799c931c0454bb5dd0c8e2fc96134a4')

pkgver() {
    cd "$srcdir/WoeUSB"
    _ver=$(git describe --long --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g')
    echo $(cut -f1-4 -d'.' <<< ${_ver})
}

prepare() {
    cd "$srcdir/WoeUSB"
    ./setup-development-environment.bash
    autoreconf --force --install
}

build() {
    cd "$srcdir/WoeUSB"
    autoconf
    ./configure --with-wx-config=wx-config-gtk3
    make
}

package() {
    depends=('wxgtk3' 'grub' 'dosfstools' 'parted' 'wget' 'ntfs-3g')
    optdepends=('zensu'
                'p7zip: For extracting EFI bootloader from Windows 7 installation media for EFI booting support')

    cd "$srcdir/WoeUSB"
    make DESTDIR="$pkgdir/" prefix="/usr" install

    # Appstream
    install -D -m644 "${srcdir}/com.woeusbgui.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.woeusbgui.metainfo.xml"
    install -Dm 644 "${srcdir}/WoeUSB/COPYING" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    mv "${pkgdir}/usr/share/applications/woeusbgui.desktop" "${pkgdir}/usr/share/applications/com.woeusbgui.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/woeusbgui-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/woeusbgui.png"
    done
}
